package ;

import dart.html.CanvasElement;
import dart.html.CanvasRenderingContext2D;
import dart.html.Element;
import dart.html.InputElement;
//import js.Browser;

using dart.Html;
using Math;
@:library("dart:math") //TEMP HACK to force imports
class Sunflower
{
    static inline var ORANGE = "orange";
    static inline var SEED_RADIUS = 2;
    static inline var SCALE_FACTOR = 4;
    static  var TAU = Math.PI * 2;

    static var MAX_D = 300;
    static var centerX = MAX_D / 2;
    static var centerY = centerX;

    static var slider:InputElement;
    static var notes:Element;
    static var PHI:Float;
    static var seeds = 0;
    static var context:CanvasRenderingContext2D;


    public static function main()
    {
        slider = cast "#slider".query();
        notes = "#notes".query();
        PHI = (5.sqrt() + 1) / 2;
        context = cast("#canvas".query(), CanvasElement).context2D;

        slider.onChange.listen(function(e) draw());
        draw();
    }

    static function draw()
    {
        seeds = Std.parseInt(slider.value);
        context.clearRect(0, 0, MAX_D, MAX_D);

        for(i in 0 ... seeds)
        {
            var theta = i * TAU / PHI;
            var r = i.sqrt() * SCALE_FACTOR;
            drawSeed(centerX + r * theta.cos(), centerY - r * theta.sin());
        }

        notes.text = Std.string(seeds) + ' seeds';
    }

    static function drawSeed(x, y)
    {
        context.beginPath();    //TODO(av) work out how to output this as method cascade and consider macro util to allow similar syntax in haxe
        context.lineWidth = 2;
        context.fillStyle = ORANGE;
        context.strokeStyle = ORANGE;
        context.arc(x, y, SEED_RADIUS, 0, TAU, false);
        context.fill();
        context.closePath();
        context.stroke();
    }
}
