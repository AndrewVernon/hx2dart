import 'dart:math';
import 'dart:html';
main()=>Sunflower.main();
class Sunflower
{
	static var TAU = PI * 2;
	static var MAX_D = 300;
	static var centerX = MAX_D / 2;
	static var centerY = centerX;
	static var slider;
	static var notes;
	static var PHI;
	static var seeds = 0;
	static var context;
	static main (  ) {
		slider = query("#slider");
		notes = query("#notes");
		PHI = (sqrt(5) + 1) / 2;
		context = (query("#canvas") as CanvasElement).context2D;
		slider.onChange.listen(( e ) {
			draw();
		});
		draw();
	}
	static draw (  ) {
		seeds = int.parse(slider.value);
		context.clearRect(0, 0, MAX_D, MAX_D);
		{
			var _g1 = 0, _g = seeds;
			while((_g1 < _g)) {
				var i = _g1++;
				var theta = i * TAU / PHI;
				var r = sqrt(i) * 4;
				drawSeed(centerX + r * cos(theta), centerY - r * sin(theta));
			};
		};
		notes.text = seeds.toString() + " seeds";
	}
	static drawSeed ( x,y ) {
		context.beginPath();
		context.lineWidth = 2;
		context.fillStyle = "orange";
		context.strokeStyle = "orange";
		context.arc(x, y, 2, 0, TAU, false);
		context.fill();
		context.closePath();
		context.stroke();
	}
	
}

