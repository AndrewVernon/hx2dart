package ;

import dart.Html;
import haxe.Timer;
import dart.html.Point;
import dart.html.CanvasRenderingContext2D;
import dart.html.Element;
import dart.html.CanvasElement;

using Math;
using dart.Html;

@:library("dart:html", "dart:math", "dart:async") //TEMP HACK
class Main
{
    static var notes:Element;
    static var fpsAverage:Float;

    public static function main()
    {
        var canvas:CanvasElement =cast "#area".query();
        Html.window.setImmediate(new SolarSystem(canvas).start);
//        Timer.delay(new SolarSystem(canvas).start, 0);  //no setImediate in haxejs api.

        notes = "#fps".query();
    }

    public static function showFps(fps:Float)
    {
        if (fpsAverage == null) fpsAverage = fps;
        fpsAverage = fps * 0.05 + fpsAverage * 0.95;
        notes.text = Std.string(fpsAverage.round()) + ' fps';
    }
}

class SolarSystem
{
    var canvas:CanvasElement;
    var width:Float;
    var height:Float;

    var sun:PlanetaryBody;

    public var renderTime:Float;

    public function new(canvas)
    {
        this.canvas = canvas;
    }

// Initialize the planets and start the simulation.
    public function start()
    {
        // Measure the canvas element.
       // var rect = canvas.parentElement.client;
        width = canvas.parent.clientWidth;
        height = canvas.parent.clientHeight;
        canvas.width = Std.int(width);

        // Create sun.
        var mercury = new PlanetaryBody(this, "orange", 0.382, 0.387, 0.241);
        var venus   = new PlanetaryBody(this, "green", 0.949, 0.723, 0.615);
        sun = new PlanetaryBody(this, "#ff2", 14.0);
        sun.addPlanet(mercury);
        sun.addPlanet(venus);

        var earth = new PlanetaryBody(this, "#33f", 1.0, 1.0, 1.0);
        var moon  = new PlanetaryBody(this, "gray", 0.2, 0.14, 0.075);
        var mars  = new PlanetaryBody(this, "red", 0.532, 1.524, 1.88);
        earth.addPlanet(moon);
        earth.addPlanet(mars);
        sun.addPlanet(earth);

        addAsteroidBelt(sun, 150);

        var f = 0.1;
        var h = 1 / 1500.0;
        var g = 1 / 72.0;

        var jupiter  = new PlanetaryBody(this, "gray", 4.0, 5.203, 11.86);
        var io       = new PlanetaryBody(this, "gray", 3.6*f, 421*h, 1.769*g);
        var europa   = new PlanetaryBody(this, "gray", 3.1*f, 671*h, 3.551*g);
        var ganymede = new PlanetaryBody(this, "gray", 5.3*f, 1070*h, 7.154*g);
        var callisto = new PlanetaryBody(this, "gray", 4.8*f, 1882*h, 16.689*g);

        jupiter.addPlanet(io);
        jupiter.addPlanet(europa);
        jupiter.addPlanet(ganymede);
        jupiter.addPlanet(callisto);

        sun.addPlanet(jupiter);
        requestRedraw();
    }

    public function draw(_:Float)
    {
        var time = Date.now().getTime();
        if (renderTime != null) Main.showFps(1000 / (time - renderTime));
        renderTime = time;

        var context = canvas.context2D;
        drawBackground(context);
        drawPlanets(context);
        requestRedraw();

        //return
    }

    function drawBackground(context:CanvasRenderingContext2D ) {
        context.clearRect(0, 0, width, height);
    }

    function drawPlanets( context:CanvasRenderingContext2D) {
        sun.draw(context, new Point(width / 2, height / 2));
    }

    function requestRedraw() {
        Html.window.requestAnimationFrame(draw);
    }

    function addAsteroidBelt(body:PlanetaryBody, count:Int ) {

    // Asteroids are generally between 2.06 and 3.27 AUs.
        for (i in 0 ... count)
        {
            var radius = 2.06 + Math.random() * (3.27 - 2.06);
            body.addPlanet(
            new PlanetaryBody(this, "#777",
            0.1 * Math.random(), radius, radius * 2));
        }
    }

    public function normalizeOrbitRadius(r:Float) return r * (width / 10.0);

    public function normalizePlanetSize(r:Float) return (r + 1).log() * (width / 100.0);
}

/**
 * A representation of a plantetary body.
 * This class can calculate its position for a given time index,
 * and draw itself and any child planets.
 */
class PlanetaryBody {
    var color:String ;
    var orbitPeriod:Float;
    var solarSystem:SolarSystem ;

    var bodySize:Float;
    var orbitRadius:Float;
    var orbitSpeed:Float;

    var planets :Array<PlanetaryBody>;//  = <PlanetaryBody>[];

    public function new(solarSystem, color, bodySize, orbitRadius = 0.0, orbitPeriod = 0.0)
    {
        this.solarSystem = solarSystem;
        this.color = color;
//        this.bodySize = bodySize;
//        this.orbitRadius = orbitRadius;
//        this.orbitPeriod = orbitPeriod;   unused

        planets = [];

        this.bodySize = solarSystem.normalizePlanetSize(bodySize);
        this.orbitRadius = solarSystem.normalizeOrbitRadius(orbitRadius);
        this.orbitSpeed = calculateSpeed(orbitPeriod);
    }

    public function addPlanet(planet:PlanetaryBody)
    {
        planets.push(planet);
    }

    public function draw(context:CanvasRenderingContext2D,  p:Point)
    {
        var pos:Point = calculatePos(p);

        drawSelf(context, pos);
        drawChildren(context, pos);
    }

    function drawSelf(context:CanvasRenderingContext2D,p:Point)
    {
        // Check for clipping.
        if (p.x + bodySize < 0 || p.x - bodySize >= context.canvas.width) return;
        if (p.y + bodySize < 0 || p.y - bodySize >= context.canvas.height) return;

        // Draw the figure.
        context.lineWidth = 0.5;
        context.fillStyle = color;
        context.strokeStyle = color;

        if (bodySize >= 2.0)
        {
            context.shadowOffsetX = 2;
            context.shadowOffsetY = 2;
            context.shadowBlur = 2;
            context.shadowColor = "#ddd";
        }

        context.beginPath();
        context.arc(p.x, p.y, bodySize, 0, Math.PI * 2, false);
        context.fill();
        context.closePath();

        context.shadowOffsetX = 0;
        context.shadowOffsetY = 0 ;
        context.shadowBlur = 0;

        context.beginPath();
        context.arc(p.x, p.y, bodySize, 0, Math.PI * 2, false);
        context.fill();
        context.closePath();
        context.stroke();
    }

    function drawChildren(context:CanvasRenderingContext2D ,p: Point)
    {
        for (planet in planets)
            planet.draw(context, p);
    }

    function calculateSpeed(period:Float ):Float
    {
        if(period == 0) return 0;
        return 1 / (60 * 24 * 2 * period);
    }

    function calculatePos(p:Point):Point
    {
        if (orbitSpeed == 0.0) return p;
        var angle:Float = solarSystem.renderTime * orbitSpeed;

        return new Point(orbitRadius * angle.cos() + p.x, orbitRadius * angle.sin() + p.y);
    }
}

