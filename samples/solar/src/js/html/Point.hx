package js.html;     //AV had to patch Point for js as chrome and firefox don't seem to have a Point class for js
#if hxjs2dart

@:native("Point")
@:library("dart:html")
extern class Point
{
	var x : Float;

	var y : Float;

	/** <p>Creates a new <code>Point</code> object.</p>
<pre>let p = new Point(x, y);
</pre>
<p>The new point, <code>p</code>, has the specified X&nbsp;and Y&nbsp;coordinates.</p> */
	function new(?arg0 : Dynamic, ?arg1 : Dynamic) : Void;

}

#else
class Point
{
    public var x:Float;
    public var y:Float;
    public function new(x, y)
    {
        this.x = x;
        this.y = y;
    }
}
#end
