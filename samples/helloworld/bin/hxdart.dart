import 'dart:html';
 main (  ) {
		var sampleText = query("#sample_text_id");
		sampleText.text = "Click me!";
		sampleText.onClick.listen(reverseText);
	}
 reverseText ( e ) {
		var text = query("#sample_text_id").text;
		var buffer = new StringBuffer();
		var i = text.length;
		while((i-- > 0)) buffer.write(text[i]);
		query("#sample_text_id").text = buffer.toString();
	}
