package dart.html;


@:library("dart:html")
@:native("Element")
@:publicFields
@:remove
extern class Element extends Node
{
    function new();

    var text:String;

    var style:Dynamic; //TODO(av)

    var onClick:Dynamic;
    var onChange:Dynamic;
    var clientWidth:Int;
    var clientHeight:Int;
//    var onChange:ElementStream<Event>;

    function getBoundingClientRect():Dynamic; //TODO(av)
}
