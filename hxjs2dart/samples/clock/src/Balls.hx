package ;

import js.html.ImageElement;
import js.html.Element;
import js.html.DivElement;
import js.Browser;
import Clock;

using Lambda;
using Math;
class Balls
{
    public static var clientWidth(get, null):Float;
    static function get_clientWidth() return js.Browser.window.innerWidth;

    public static var clientHeight(get, null):Float;
    static function get_clientHeight() return js.Browser.window.innerHeight;

    public static var RADIUS2:Float = Ball.RADIUS * Ball.RADIUS;

    public static var LT_GRAY_BALL_INDEX = 0;
    public static var GREEN_BALL_INDEX = 1;
    public static var BLUE_BALL_INDEX = 2;

    public static var DK_GRAY_BALL_INDEX = 4;
    public static var RED_BALL_INDEX = 5;
    public static var MD_GRAY_BALL_INDEX = 6;

    public static var PNGS = [
        "images/ball-d9d9d9.png", "images/ball-009a49.png",
        "images/ball-13acfa.png", "images/ball-265897.png",
        "images/ball-b6b4b5.png", "images/ball-c0000b.png",
        "images/ball-c9c9c9.png"
    ];

    var root:DivElement;
    var lastTime:Float;
    var balls:Array<Ball>;

    public function new()
    {
        lastTime = Date.now().getTime();
        balls = [];

        root = Browser.document.createDivElement();
        Browser.document.body.appendChild(root);
        Clock.makeAbsolute(root);
        Clock.setElementSize(root, 0.0, 0.0, 0.0, 0.0);
    }

    public function tick(now:Float)
    {
        Clock.showFps(1000.0 / (now - lastTime + 0.01));

        var delta = Math.min((now - lastTime) / 1000.0, 0.1);
        lastTime = now;

        // incrementally move each ball, removing balls that are offscreen
        balls = balls.filter(function(ball) return ball.tick(delta));  //TODO(av) work out thi

        collideBalls(delta);
    }

    function collideBalls(delta:Float)
    {
        balls.iter(function(b0) {
            balls.iter(function(b1) {
            // See if the two balls are intersecting.
            var dx = (b0.x - b1.x).abs();
            var dy = (b0.y - b1.y).abs();
            var d2 = dx * dx + dy * dy;

            if (d2 < RADIUS2)
            {
                // Make sure they're actually on a collision path
                // (not intersecting while moving apart).
                // This keeps balls that end up intersecting from getting stuck
                // without all the complexity of keeping them strictly separated.
                if (newDistanceSquared(delta, b0, b1) > d2)
                {
                    return;
                }

            // They've collided. Normalize the collision vector.
                var d = d2.sqrt();

                if (d == 0) {
                // TODO: move balls apart.
                    return;
                }

                dx /= d;
                dy /= d;

                // Calculate the impact velocity and speed along the collision vector.
                var impactx:Float = b0.vx - b1.vx;
                var impacty:Float = b0.vy - b1.vy;
                var impactSpeed:Float = impactx * dx + impacty * dy;

                // Bump.
                b0.vx -= dx * impactSpeed;
                b0.vy -= dy * impactSpeed;
                b1.vx += dx * impactSpeed;
                b1.vy += dy * impactSpeed;
                }
            });
        });
    }

    function newDistanceSquared(delta:Float, b0:Ball, b1:Ball):Float
    {
        var nb0x = b0.x + b0.vx * delta;
        var nb0y = b0.y + b0.vy * delta;
        var nb1x = b1.x + b1.vx * delta;
        var nb1y = b1.y + b1.vy * delta;
        var ndx = (nb0x - nb1x).abs();
        var ndy = (nb0y - nb1y).abs();
        var nd2 = ndx * ndx + ndy * ndy;
        return nd2;
    }

    public function add(x:Float, y:Float, color:Int)
    {
        balls.push(new Ball(root, x, y, color));
    }
}

class Ball
{
    static var GRAVITY = 400.0;
    static var RESTITUTION = 0.8;
    static var MIN_VELOCITY = 100.0;
    static var INIT_VELOCITY = 800.0;
    public static var RADIUS = 14.0;

    //static Random random;

    static function randomVelocity()
    {
        return (Math.random() - 0.5) * INIT_VELOCITY;
    }

    var root:Element;
    var elem:ImageElement;
    public var x:Float;
    public var y:Float;
    public var vx:Float;
    public var vy:Float;
    public var ax:Float;
    public var ay:Float;
    public var age:Float;

    public function new(root, x, y, color:Int)
    {
        this.root = root;
        this.x = x;
        this.y = y;

        elem = Browser.document.createImageElement();
        elem.src = Balls.PNGS[color];
        Clock.makeAbsolute(elem);
        Clock.setElementPosition(elem, x, y);
        root.appendChild(elem);

        ax = 0.0;
        ay = GRAVITY;

        vx = randomVelocity();
        vy = randomVelocity();
    }

    // return false => remove me
    public function tick(delta:Float):Bool
    {
        // Update velocity and position.
        vx += ax * delta;
        vy += ay * delta;

        x += vx * delta;
        y += vy * delta;

        // Handle falling off the edge.
        if ((x < RADIUS) || (x > Balls.clientWidth)) {
            elem.remove();
            return false;
        }

        // Handle ground collisions.
        if (y > Balls.clientHeight)
        {
            y = Balls.clientHeight;
            vy *= -RESTITUTION;
        }

        // Position the element.
        Clock.setElementPosition(elem, x - RADIUS, y - RADIUS);

        return true;
    }
}

