(function () { "use strict";
var Main = function() { }
Main.__name__ = true;
Main.main = function() {
	var canvas = js.Browser.document.getElementById("area");
	haxe.Timer.delay(($_=new SolarSystem(canvas),$bind($_,$_.start)),0);
	Main.notes = js.Browser.document.getElementById("fps");
}
Main.showFps = function(fps) {
	if(Main.fpsAverage == null) Main.fpsAverage = fps;
	Main.fpsAverage = fps * 0.05 + Main.fpsAverage * 0.95;
	Main.notes.innerText = Std.string(Math.round(Main.fpsAverage)) + " fps";
}
var SolarSystem = function(canvas) {
	this.canvas = canvas;
};
SolarSystem.__name__ = true;
SolarSystem.prototype = {
	normalizePlanetSize: function(r) {
		return Math.log(r + 1) * (this.width / 100.0);
	}
	,normalizeOrbitRadius: function(r) {
		return r * (this.width / 10.0);
	}
	,addAsteroidBelt: function(body,count) {
		var _g = 0;
		while(_g < count) {
			var i = _g++;
			var radius = 2.06 + Math.random() * 1.21;
			body.addPlanet(new PlanetaryBody(this,"#777",0.1 * Math.random(),radius,radius * 2));
		}
	}
	,requestRedraw: function() {
		js.Browser.window.requestAnimationFrame($bind(this,this.draw));
	}
	,drawPlanets: function(context) {
		this.sun.draw(context,new js.html.Point(this.width / 2,this.height / 2));
	}
	,drawBackground: function(context) {
		context.clearRect(0,0,this.width,this.height);
	}
	,draw: function(_) {
		var time = new Date().getTime();
		if(this.renderTime != null) Main.showFps(1000 / (time - this.renderTime));
		this.renderTime = time;
		var context = this.canvas.getContext("2d");
		this.drawBackground(context);
		this.drawPlanets(context);
		this.requestRedraw();
	}
	,start: function() {
		this.width = this.canvas.parentElement.clientWidth;
		this.height = this.canvas.parentElement.clientHeight;
		this.canvas.width = this.width | 0;
		var mercury = new PlanetaryBody(this,"orange",0.382,0.387,0.241);
		var venus = new PlanetaryBody(this,"green",0.949,0.723,0.615);
		this.sun = new PlanetaryBody(this,"#ff2",14.0);
		this.sun.addPlanet(mercury);
		this.sun.addPlanet(venus);
		var earth = new PlanetaryBody(this,"#33f",1.0,1.0,1.0);
		var moon = new PlanetaryBody(this,"gray",0.2,0.14,0.075);
		var mars = new PlanetaryBody(this,"red",0.532,1.524,1.88);
		earth.addPlanet(moon);
		earth.addPlanet(mars);
		this.sun.addPlanet(earth);
		this.addAsteroidBelt(this.sun,150);
		var f = 0.1;
		var h = 1 / 1500.0;
		var g = 1 / 72.0;
		var jupiter = new PlanetaryBody(this,"gray",4.0,5.203,11.86);
		var io = new PlanetaryBody(this,"gray",3.6 * f,421 * h,1.769 * g);
		var europa = new PlanetaryBody(this,"gray",3.1 * f,671 * h,3.551 * g);
		var ganymede = new PlanetaryBody(this,"gray",5.3 * f,1070 * h,7.154 * g);
		var callisto = new PlanetaryBody(this,"gray",4.8 * f,1882 * h,16.689 * g);
		jupiter.addPlanet(io);
		jupiter.addPlanet(europa);
		jupiter.addPlanet(ganymede);
		jupiter.addPlanet(callisto);
		this.sun.addPlanet(jupiter);
		this.requestRedraw();
	}
}
var PlanetaryBody = function(solarSystem,color,bodySize,orbitRadius,orbitPeriod) {
	if(orbitPeriod == null) orbitPeriod = 0.0;
	if(orbitRadius == null) orbitRadius = 0.0;
	this.solarSystem = solarSystem;
	this.color = color;
	this.planets = [];
	this.bodySize = solarSystem.normalizePlanetSize(bodySize);
	this.orbitRadius = solarSystem.normalizeOrbitRadius(orbitRadius);
	this.orbitSpeed = this.calculateSpeed(orbitPeriod);
};
PlanetaryBody.__name__ = true;
PlanetaryBody.prototype = {
	calculatePos: function(p) {
		if(this.orbitSpeed == 0.0) return p;
		var angle = this.solarSystem.renderTime * this.orbitSpeed;
		return new js.html.Point(this.orbitRadius * Math.cos(angle) + p.x,this.orbitRadius * Math.sin(angle) + p.y);
	}
	,calculateSpeed: function(period) {
		if(period == 0) return 0;
		return 1 / (2880 * period);
	}
	,drawChildren: function(context,p) {
		var _g = 0, _g1 = this.planets;
		while(_g < _g1.length) {
			var planet = _g1[_g];
			++_g;
			planet.draw(context,p);
		}
	}
	,drawSelf: function(context,p) {
		if(p.x + this.bodySize < 0 || p.x - this.bodySize >= context.canvas.width) return;
		if(p.y + this.bodySize < 0 || p.y - this.bodySize >= context.canvas.height) return;
		context.lineWidth = 0.5;
		context.fillStyle = this.color;
		context.strokeStyle = this.color;
		if(this.bodySize >= 2.0) {
			context.shadowOffsetX = 2;
			context.shadowOffsetY = 2;
			context.shadowBlur = 2;
			context.shadowColor = "#ddd";
		}
		context.beginPath();
		context.arc(p.x,p.y,this.bodySize,0,Math.PI * 2,false);
		context.fill();
		context.closePath();
		context.shadowOffsetX = 0;
		context.shadowOffsetY = 0;
		context.shadowBlur = 0;
		context.beginPath();
		context.arc(p.x,p.y,this.bodySize,0,Math.PI * 2,false);
		context.fill();
		context.closePath();
		context.stroke();
	}
	,draw: function(context,p) {
		var pos = this.calculatePos(p);
		this.drawSelf(context,pos);
		this.drawChildren(context,pos);
	}
	,addPlanet: function(planet) {
		this.planets.push(planet);
	}
}
var Std = function() { }
Std.__name__ = true;
Std.string = function(s) {
	return js.Boot.__string_rec(s,"");
}
var haxe = {}
haxe.Timer = function(time_ms) {
	var me = this;
	this.id = setInterval(function() {
		me.run();
	},time_ms);
};
haxe.Timer.__name__ = true;
haxe.Timer.delay = function(f,time_ms) {
	var t = new haxe.Timer(time_ms);
	t.run = function() {
		t.stop();
		f();
	};
	return t;
}
haxe.Timer.prototype = {
	run: function() {
		console.log("run");
	}
	,stop: function() {
		if(this.id == null) return;
		clearInterval(this.id);
		this.id = null;
	}
}
var js = {}
js.Boot = function() { }
js.Boot.__name__ = true;
js.Boot.__string_rec = function(o,s) {
	if(o == null) return "null";
	if(s.length >= 5) return "<...>";
	var t = typeof(o);
	if(t == "function" && (o.__name__ || o.__ename__)) t = "object";
	switch(t) {
	case "object":
		if(o instanceof Array) {
			if(o.__enum__) {
				if(o.length == 2) return o[0];
				var str = o[0] + "(";
				s += "\t";
				var _g1 = 2, _g = o.length;
				while(_g1 < _g) {
					var i = _g1++;
					if(i != 2) str += "," + js.Boot.__string_rec(o[i],s); else str += js.Boot.__string_rec(o[i],s);
				}
				return str + ")";
			}
			var l = o.length;
			var i;
			var str = "[";
			s += "\t";
			var _g = 0;
			while(_g < l) {
				var i1 = _g++;
				str += (i1 > 0?",":"") + js.Boot.__string_rec(o[i1],s);
			}
			str += "]";
			return str;
		}
		var tostr;
		try {
			tostr = o.toString;
		} catch( e ) {
			return "???";
		}
		if(tostr != null && tostr != Object.toString) {
			var s2 = o.toString();
			if(s2 != "[object Object]") return s2;
		}
		var k = null;
		var str = "{\n";
		s += "\t";
		var hasp = o.hasOwnProperty != null;
		for( var k in o ) { ;
		if(hasp && !o.hasOwnProperty(k)) {
			continue;
		}
		if(k == "prototype" || k == "__class__" || k == "__super__" || k == "__interfaces__" || k == "__properties__") {
			continue;
		}
		if(str.length != 2) str += ", \n";
		str += s + k + " : " + js.Boot.__string_rec(o[k],s);
		}
		s = s.substring(1);
		str += "\n" + s + "}";
		return str;
	case "function":
		return "<function>";
	case "string":
		return o;
	default:
		return String(o);
	}
}
js.Browser = function() { }
js.Browser.__name__ = true;
js.html = {}
js.html.Point = function(x,y) {
	this.x = x;
	this.y = y;
};
js.html.Point.__name__ = true;
var $_, $fid = 0;
function $bind(o,m) { if( m == null ) return null; if( m.__id__ == null ) m.__id__ = $fid++; var f; if( o.hx__closures__ == null ) o.hx__closures__ = {}; else f = o.hx__closures__[m.__id__]; if( f == null ) { f = function(){ return f.method.apply(f.scope, arguments); }; f.scope = o; f.method = m; o.hx__closures__[m.__id__] = f; } return f; };
Math.__name__ = ["Math"];
Math.NaN = Number.NaN;
Math.NEGATIVE_INFINITY = Number.NEGATIVE_INFINITY;
Math.POSITIVE_INFINITY = Number.POSITIVE_INFINITY;
Math.isFinite = function(i) {
	return isFinite(i);
};
Math.isNaN = function(i) {
	return isNaN(i);
};
String.__name__ = true;
Array.__name__ = true;
Date.__name__ = ["Date"];
js.Browser.window = typeof window != "undefined" ? window : null;
js.Browser.document = typeof window != "undefined" ? window.document : null;
Main.main();
})();
