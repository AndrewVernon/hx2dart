(function ()
{
    function l()
    {
    }

    function f(a, b, c, e, d)
    {
        null == d && (d = 0);
        null == e && (e = 0);
        this.L = a;
        this.color = b;
        this.r = [];
        this.d = a.I(c);
        this.p = a.H(e);
        this.q = this.w(d)
    }

    function m(a)
    {
        this.canvas = a
    }

    function h()
    {
    }

    function p(a, b)
    {
        if(null == b)return null;
        null == b.m && (b.m = q ++);
        var c;
        null == a.n ? a.n = {} : c = a.n[b.m];
        null == c && (c = function ()
        {
            return c.method.apply(c.scope, arguments)
        }, c.scope = a, c.method = b, a.n[b.m] = c);
        return c
    }

    h.a = ! 0;
    h.G = function ()
    {
        k.g.A((n = new m(g.e.document.getElementById("area")), p(n, n.start)));
        h.J = g.e.document.getElementById("fps")
    };
    h.K = function (a)
    {
        null == h.j && (h.j = a);
        h.j = 0.05 * a + 0.95 * h.j;
        h.J.innerText = l.M(Math.round(h.j)) + " fps"
    };
    m.a = ! 0;
    m.prototype = {I: function (a)
    {
        return Math.log(a + 1) * (this.width / 100)
    }, H: function (a)
    {
        return a * (this.width / 10)
    }, u: function (a, b)
    {
        for(var c = 0; c < b;)
        {
            c ++;
            var e = 2.06 + 1.21 * Math.random();
            a.b(new f(this, "#777", 0.1 * Math.random(), e, 2 * e))
        }
    }, s: function ()
    {
        g.e.window.requestAnimationFrame(p(this, this.i))
    }, D: function (a)
    {
        this.f.i(a, new g.k.l(this.width / 2, this.height / 2))
    }, B: function (a)
    {
        a.clearRect(0, 0, this.width, this.height)
    },
        i: function ()
        {
            var a = (new Date).getTime();
            null != this.o && h.K(1E3 / (a - this.o));
            this.o = a;
            a = this.canvas.getContext("2d");
            this.B(a);
            this.D(a);
            this.s()
        }, start: function ()
        {
            this.width = this.canvas.parentElement.clientWidth;
            this.height = this.canvas.parentElement.clientHeight;
            this.canvas.width = this.width | 0;
            var a = new f(this, "orange", 0.382, 0.387, 0.241), b = new f(this, "green", 0.949, 0.723, 0.615);
            this.f = new f(this, "#ff2", 14);
            this.f.b(a);
            this.f.b(b);
            var a = new f(this, "#33f", 1, 1, 1), b = new f(this, "gray", 0.2, 0.14, 0.075), c = new f(this,
                                                                                                       "red", 0.532, 1.524, 1.88);
            a.b(b);
            a.b(c);
            this.f.b(a);
            this.u(this.f, 150);
            var e = 1 / 1500, d = 1 / 72, a = new f(this, "gray", 4, 5.203, 11.86), b = new f(this, "gray", 3.6 * 0.1, 421 * e, 1.769 * d), c = new f(this, "gray", 3.1 * 0.1, 671 * e, 3.551 * d), g = new f(this, "gray", 0.53, 1070 * e, 7.154 * d), e = new f(this, "gray", 0.48, 1882 * e, 16.689 * d);
            a.b(b);
            a.b(c);
            a.b(g);
            a.b(e);
            this.f.b(a);
            this.s()
        }};
    f.a = ! 0;
    f.prototype = {v: function (a)
    {
        if(0 == this.q)return a;
        var b = this.L.o * this.q;
        return new g.k.l(this.p * Math.cos(b) + a.x, this.p * Math.sin(b) + a.y)
    }, w: function (a)
    {
        return 0 ==
               a ? 0 : 1 / (2880 * a)
    }, C: function (a, b)
    {
        for(var c = 0, e = this.r; c < e.length;)
        {
            var d = e[c];
            ++ c;
            d.i(a, b)
        }
    }, F: function (a, b)
    {
        0 > b.x + this.d || b.x - this.d >= a.canvas.width || 0 > b.y + this.d || b.y - this.d >= a.canvas.height || (a.lineWidth = 0.5, a.fillStyle = this.color, a.strokeStyle = this.color, 2 <= this.d && (a.shadowOffsetX = 2, a.shadowOffsetY = 2, a.shadowBlur = 2, a.shadowColor = "#ddd"), a.beginPath(), a.arc(b.x, b.y, this.d, 0, 2 * Math.PI, ! 1), a.fill(), a.closePath(), a.shadowOffsetX = 0, a.shadowOffsetY = 0, a.shadowBlur = 0, a.beginPath(), a.arc(b.x, b.y, this.d,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                           0, 2 * Math.PI, ! 1), a.fill(), a.closePath(), a.stroke())
    }, i: function (a, b)
    {
        var c = this.v(b);
        this.F(a, c);
        this.C(a, c)
    }, b: function (a)
    {
        this.r.push(a)
    }};
    l.a = ! 0;
    l.M = function (a)
    {
        return g.c.h(a, "")
    };
    var k = {g: function (a)
    {
        var b = this;
        this.id = setInterval(function ()
                              {
                                  b.t()
                              }, a)
    }};
    k.g.a = ! 0;
    k.g.A = function (a)
    {
        var b = new k.g(0);
        b.t = function ()
        {
            b.stop();
            a()
        }
    };
    k.g.prototype = {t: function ()
    {
        console.log("run")
    }, stop: function ()
    {
        null != this.id && (clearInterval(this.id), this.id = null)
    }};
    var g = {c: function ()
    {
    }};
    g.c.a = ! 0;
    g.c.h = function (a, b)
    {
        if(null ==
           a)return"null";
        if(5 <= b.length)return"<...>";
        var c = typeof a;
        "function" == c && (a.a || a.N) && (c = "object");
        switch(c)
        {
            case "object":
                if(a instanceof Array)
                {
                    if(a.O)
                    {
                        if(2 == a.length)return a[0];
                        c = a[0] + "(";
                        b += "\t";
                        for(var e = 2, d = a.length; e < d;)var f = e ++, c = 2 != f ? c + ("," + g.c.h(a[f], b)) : c + g.c.h(a[f], b);
                        return c + ")"
                    }
                    e = a.length;
                    c = "[";
                    b += "\t";
                    for(d = 0; d < e;)f = d ++, c += (0 < f ? "," : "") + g.c.h(a[f], b);
                    return c + "]"
                }
                try
                {
                    d = a.toString
                }
                catch(h)
                {
                    return"???"
                }
                if(null != d && d != Object.toString && (c = a.toString(), "[object Object]" != c))return c;
                d =
                null;
                c = "{\n";
                b += "\t";
                e = null != a.hasOwnProperty;
                for(d in a)e && ! a.hasOwnProperty(d) || "prototype" == d || "__class__" == d || "__super__" == d || "__interfaces__" == d || "__properties__" == d || (2 != c.length && (c += ", \n"), c += b + d + " : " + g.c.h(a[d], b));
                b = b.substring(1);
                return c += "\n" + b + "}";
            case "function":
                return"<function>";
            case "string":
                return a;
            default:
                return String(a)
        }
    };
    g.e = function ()
    {
    };
    g.e.a = ! 0;
    g.k = {};
    g.k.l = function (a, b)
    {
        this.x = a;
        this.y = b
    };
    g.k.l.a = ! 0;
    var n, q = 0;
    Math.a = ["Math"];
    Math.NaN = Number.NaN;
    Math.NEGATIVE_INFINITY =
    Number.NEGATIVE_INFINITY;
    Math.POSITIVE_INFINITY = Number.POSITIVE_INFINITY;
    Math.P = function (a)
    {
        return isFinite(a)
    };
    Math.Q = function (a)
    {
        return isNaN(a)
    };
    String.a = ! 0;
    Array.a = ! 0;
    Date.a = ["Date"];
    g.e.window = "undefined" != typeof window ? window : null;
    g.e.document = "undefined" != typeof window ? window.document : null;
    h.G()
})();